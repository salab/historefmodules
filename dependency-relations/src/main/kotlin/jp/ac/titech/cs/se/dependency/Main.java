package jp.ac.titech.cs.se.dependency;

import jp.ac.titech.cs.se.commit2changes.Converter;
import jp.ac.titech.cs.se.dependency.helper.SOBAHelperKt;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;
import soba.core.ClassInfo;
import soba.core.JavaProgram;
import soba.core.MethodInfo;
import soba.core.method.CallSite;
import soba.core.vta.CallResolver;
import soba.util.IntPairProc;
import soba.util.files.ClasspathUtil;

import java.io.File;
import java.io.IOException;

/**
 * Created by jmatsu on 2015/10/04.
 */
public class Main {
    public static void main(String[] args) throws IOException {
        String path = "/Users/jmatsu/Projects/salab/floss/git";
        String refs = "refs/heads/master";
        int depth = 3;

//        Repository repository = new FileRepositoryBuilder().findGitDir(new File(path)).build();
//        new Converter(repository, refs).change().beginFromHead(depth).forEach(System.out::println);

        String[] classes = new String[]{"/Users/jmatsu/Projects/salab/historef/java/data_dependency/build/classes/main"};

        JavaProgram program = new JavaProgram(ClasspathUtil.getClassList(classes));
        CallResolver resolver = CallResolver.getVTA(program);

        for (ClassInfo c: program.getClasses()) {
            for (MethodInfo m: c.getMethods()) {
                if (m.getMethodName().equals("sample")) {
                    SOBAHelperKt.buildDataDependency(m.getDataDependence(), "a");
                }
            }
        }
    }
}


