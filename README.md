# Historef modules

[![Release](https://img.shields.io/github/release/jitpack/gradle-simple.svg?label=gradle)](https://jitpack.io/#org.bitbucket.salab/historefmodules)

Modules for Historef.

The aim is to make historef be adaptable and independent system.
This is NOT UI components.

Read followings,

* This should not contain UI modules.
* This should use observe or observe-like pattern if user interaction is needed.

## Usage

# Add jitpack.io as Maven central repository.
```
repositories {
    // other repositories.  e.g. jcenter()
    maven { url "https://jitpack.io" }
}
```

# Add the dependency.

```
dependencies {
    // Each name of published tags represents ${release_version}.
    // You can choose one of them and use it.

    // if you want to use all of modules.
    compile "org.bitbucket.salab:historefmodules:${release_version}"

    // if you want to choose one module and use it.
    compile "org.bitbucket.salab.historefmodules:${module_name}:${release_version}"
    //                          ↑ this is important!!!
}
```